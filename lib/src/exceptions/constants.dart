class ExceptionConstants {
  static String internalServerError = 'Internal Server Error';

  static String forbidden = 'Forbidden;';

  static String badRequest = 'Bad Request';

  static String unauthorized = 'Not Authorized';

}